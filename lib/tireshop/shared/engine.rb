module Tireshop
  module Shared
    class Engine < ::Rails::Engine
      isolate_namespace Tireshop::Shared
    end
  end
end
