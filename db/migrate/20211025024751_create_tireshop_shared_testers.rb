class CreateTireshopSharedTesters < ActiveRecord::Migration[6.0]
  def change
    create_table :tireshop_shared_testers do |t|
      t.string :name

      t.timestamps
    end
  end
end
